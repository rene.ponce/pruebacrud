import { Information } from "./information";

export interface User {
  id?: number;
  name: string;
  email: string;
  password: string;
  information: Information
}
