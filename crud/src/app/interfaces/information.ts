export interface Information {
  user_id: number;
  address: string;
  telephone: string;
  birthday: string;
  age: number;
}
