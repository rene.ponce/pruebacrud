import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/interfaces/user';
import { CrudService } from '../../services/crud.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  
  user!: User;
  formUser!: FormGroup;

  constructor(private activedRoute: ActivatedRoute,
              private crudService: CrudService,
              private fb: FormBuilder,
              private router: Router) { }

  ngOnInit(): void {
    this.activedRoute.params.subscribe(param => {
      let id = param['id'];
      this.getUser(id);
    });
  }

  getUser(id: number) {
    this.crudService.getUser(id).subscribe((response: any) => {
      this.user = response;
      this.initialize();
    }, error => console.log(error));
  }

  initialize() {
    this.formUser = this.fb.group({
      name: [this.user.name, Validators.required],
      email: [this.user.email, [Validators.required, Validators.email]],
      address: [this.user.information.address, Validators.required],
      telephone: [this.user.information.telephone, [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      birthday: [this.user.information.birthday, Validators.required]
    });
  }

  updateUser() {
    this.crudService.updateUser(this.formUser.value, this.user.id!).subscribe(response => {
      alert("Usuario actualizado exitosamente");
      this.router.navigateByUrl('list');
    }, error => console.log(error));
  }

}
