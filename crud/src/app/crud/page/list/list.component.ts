import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { CrudService } from '../../services/crud.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  users: User[] = [];

  constructor(private crudService: CrudService) { }

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers() {
    this.crudService.getAllUsers().subscribe(response => {
      this.users = response;
    }, error => {
      console.log(error);
    });
  }

  deleteUser(id: number) {
    this.crudService.deleteUser(id).subscribe(response => {
      this.getUsers();
    }, error => console.log(error));
  }

}
