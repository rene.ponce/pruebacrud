import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(private http: HttpClient) { }

  getAllUsers(): Observable<any> {
    return this.http.get(`${environment.api}users`).pipe(
      map((data) => {
        return data;
      })
    );
  }

  getUser(id: number) {
    return this.http.get(`${environment.api}users/${id}/edit`).pipe(
      map((data) => {
        return data;
      })
    );
  }

  updateUser(data: any, id: number) {
    return this.http.put(`${environment.api}users/${id}`, data);
  }

  deleteUser(id: number) {
    return this.http.delete(`${environment.api}users/${id}`);
  }
}
