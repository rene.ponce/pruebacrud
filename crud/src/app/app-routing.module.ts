import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './crud/page/list/list.component';

const routes: Routes = [
  {
    path: 'list',
    loadChildren: () => import('./crud/crud.module').then(m => m.CrudModule)
  },
  {
    path: '**',
    redirectTo: 'list'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
