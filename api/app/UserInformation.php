<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserInformation extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'address', 'telephone', 'birthday'
    ];

    protected $appends = ['age'];

    public function user()
    {
        return $this->belongsTo('\App\User', 'id', 'user_id');
    }

    public function getAgeAttribute()
    {
        return Carbon::parse($this->attributes['birthday'])->age;
    }
}
