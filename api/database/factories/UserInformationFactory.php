<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\UserInformation;
use Faker\Generator as Faker;

$factory->define(UserInformation::class, function (Faker $faker) {
    return [
        'user_id' => factory(\App\User::class)->create()->id,
        'address' => $faker->address,
        'telephone' => $faker->phoneNumber,
        'birthday' => $faker->date('Y-m-d', 'now')
    ];
});
