<?php

use Illuminate\Database\Seeder;

class UserInformationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        for ($i = 1; $i <= 100; $i++) {
            \App\UserInformation::create([
                'user_id' => $i,
                'address' => $faker->address,
                'telephone' => $faker->phoneNumber,
                'birthday' => $faker->date('Y-m-d', 'now')
            ]);
        }
    }
}
