# Proyecto api

Las apis están desarrolladas en PHP con el framework Laravel 6, se requerie tener instalado lo siguiente:

 - Composer versión 1.9.*
 - PHP Versión 7.4 o superior

Una vez instalado composer dentro del folder api ejecutar

    composer install
Una vez que  se instaló el framework y sus dependencias hay que renombrar el archivo **.env.example** por **.env** dentro de este archivo se configuran las credenciales de la base de datos. Se crea la base de datos, en este caso se usó MySQL y ya con la base de datos creada ejecutamos los siguientes comandos.

    php artisan key:generate
    php artisan migrate --seed
Una vez terminado de ejecutar los comandos, procedemos a levantar los servicios con el siguiente comando

    php artisan serve

# Proyecto CRUD

El frontend está desarrollado en angular, por lo que una vez dentro de la carpeta crud hay que ejecutar el siguiente comando.

    npm install
Una vez que se instalan las dependencias procedemos a levantar el proyecto con el siguiente comando

    ng serve
**IMPORTANTE**
Si las apis fueron levantadas en un puerto diferente del 8000 o en otra URL distinta del localhost hay que hacer el cambio en la llave **api** del archivo **environment.ts** dentro del folder **environments**

En caso de no tener instalado angular ejecutar el siguiente comando

    npm i -g @angular/cli
Si se ejecuta en un SO unix o macOS anteponer la palabra **sudo** 
